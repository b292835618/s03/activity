USE music_db;

ALTER TABLE reviews DROP COLUMN review;
ALTER TABLE reviews DROP COLUMN feedback;
ALTER TABLE reviews ADD reviews VARCHAR(500) NOT NULL AFTER id;
ALTER TABLE reviews ADD rating INT NOT NULL AFTER datetime_created;

INSERT INTO reviews (review, datetime_created, rating) VALUES ("The songs are okay. Worth the subscription", "2023-05-03 00:00:00", 5);
INSERT INTO reviews (review, datetime_created, rating) VALUES ("The songs are meh. I want BLACKPINK", "2023-01-23 00:00:00", 1);
INSERT INTO reviews (review, datetime_created, rating) VALUES ("Add Bruno Mars and Lady Gaga", "2023-03-03 00:00:00", 4);
INSERT INTO reviews (review, datetime_created, rating) VALUES ("I want to listen to more k-pop", "2022-09-23 00:00:00", 3);
INSERT INTO reviews (review, datetime_created, rating) VALUES ("Kindly add more OPM", "2023-02-01 00:00:00", 5);

-- displaying all review records
SELECT * FROM reviews;

-- displaying all review records with the rating of 5
SELECT * FROM reviews WHERE rating = 5;

-- displaying all review records with the rating of 1
SELECT * FROM reviews WHERE rating = 1;

-- Updating all ratings to 5
UPDATE reviews SET rating = 5 WHERE rating < 5;

-- creating a new db called course_db
CREATE DATABASE course_db;

--Drop a database
DROP DATABASE course_db;

--Recreate a database
CREATE DATABASE course_db;

CREATE TABLE students(
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL,
	email VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE subjects(
	id INT NOT NULL AUTO_INCREMENT,
	course_name VARCHAR(100) NOT NULL,
	schedule VARCHAR(100) NOT NULL,
	instructor VARCHAR(100) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE enrollments(
	id INT NOT NULL AUTO_INCREMENT,
	student_id INT NOT NULL,
	subject_id INT NOT NULL,
	datetime_created DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_enrollments_student_id
		FOREIGN KEY (student_id) REFERENCES students(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_enrollments_subject_id
		FOREIGN KEY (subject_id) REFERENCES subjects(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

INSERT INTO subjects (course_name, schedule, instructor) VALUES ("Sorcery", "5PM-10PM", "Ms. Camille");

INSERT INTO students (username, password, full_name, email) VALUES ("lulusupport", "lulu", "Lulu Yordle", "lulu@school.com");

INSERT INTO enrollments (student_id, subject_id, datetime_created) VALUES (1, 1, "2023-05-03 20:42:50");